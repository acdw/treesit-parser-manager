;;; treesit-parser-manager.el --- Manage tree-sitter grammars. -*- lexical-binding: t -*-

;; Copyright (C) 2022-2022  Christian Kruse <christian@kruse.cool>

;; Author: Christian Kruse <christian@kruse.cool>
;; URL: https://codeberg.org/ckruse/treesit-parser-manager
;; Version: 0.1.0
;; Package-Requires: ((emacs "29.1"))
;; Keywords: treesit, tree-sitter, grammar, parser, language

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; With this mode, you can manage tree-sitter grammars.  It allows you to
;; install, update and remove tree-sitter grammars.

;;; Code:

(require 'subr-x)
(require 'json)

(defcustom treesit-parser-manager-working-directory (expand-file-name "tree-sit-repos" user-emacs-directory)
  "Directory where tree-sit-parser-manager will clone repositories."
  :type 'directory
  :group 'treesit-parser-manager)

(defcustom treesit-parser-manager-clone-command "git clone --depth 1"
  "Command used to clone repositories."
  :type 'string
  :group 'treesit-parser-manager)

(defcustom treesit-parser-manager-update-command "git pull"
  "Command used to update repositories."
  :type 'string
  :group 'treesit-parser-manager)

(defcustom treesit-parser-manager-target-directory (expand-file-name "tree-sit-parsers" user-emacs-directory)
  "Directory where tree-sit-parser-manager will install parsers."
  :type 'directory
  :group 'treesit-parser-manager)

(defcustom treesit-parser-manager-grammars '()
  "List of grammars to install."
  :type '(repeat (list :tag "Grammar"
                       (string :tag "Repository")
                       (repeat :tag "Grammars"
                               (string :tag "Grammar"))))
  :group 'treesit-parser-manager)

(defcustom treesit-parser-manager-c-compiler "cc"
  "C Compiler to build the grammars"
  :type 'string
  :group 'treesit-parser-manager)

(defcustom treesit-parser-manager-cflags '()
  "CFLAGS to build the grammars"
  :type '(repeat string)
  :group 'treesit-parser-manager)

(defcustom treesit-parser-manager-c++-compiler "c++"
  "C++ Compiler to build the grammars"
  :type 'string
  :group 'treesit-parser-manager)

(defun treesit-parser-manager--clone (repo)
  "Clone REPO."
  (make-directory treesit-parser-manager-working-directory 'parents)
  (let ((default-directory treesit-parser-manager-working-directory))
    (shell-command (format "%s %s" treesit-parser-manager-clone-command repo))))

(defun treesit-parser-manager--update (repo)
  "Update REPO."
  (let ((default-directory (expand-file-name repo treesit-parser-manager-working-directory)))
    (shell-command treesit-parser-manager-update-command)))

;;;###autoload
(defun treesit-parser-manager-install-grammars ()
  "Install tree-sitter grammars."
  (interactive)
  (dolist (gramar treesit-parser-manager-grammars)
    (let ((url (car gramar))
          (targets (nth 1 gramar)))
      (dolist (target targets)
        (let ((target-dir (expand-file-name target treesit-parser-manager-working-directory)))
          (unless (file-exists-p target-dir)
            (message "Installing %s (%s)" url target)
            (treesit-parser-manager--clone url)
            (treesitter-grammar-manager--compile-grammar treesit-parser-manager-target-directory target-dir)))))))

;;;###autoload
(defun treesit-parser-manager-update-grammars ()
  "Update tree-sitter grammars."
  (interactive)
  (dolist (gramar treesit-parser-manager-grammars)
    (let ((url (car gramar))
          (targets (nth 1 gramar)))
      (dolist (target targets)
        (let ((target-dir (expand-file-name target treesit-parser-manager-working-directory)))
          (when (file-exists-p target-dir)
            (message "Updating %s (%s)" url target)
            (treesit-parser-manager--update target-dir)
            (treesitter-grammar-manager--compile-grammar treesit-parser-manager-target-directory target-dir)))))))

;;;###autoload
(defun treesit-parser-manager-install-or-update-grammars ()
  "Compile or update tree-sitter grammars."
  (interactive)
  (dolist (gramar treesit-parser-manager-grammars)
    (let ((url (car gramar))
          (targets (nth 1 gramar)))
      (dolist (target targets)
        (let ((target-dir (expand-file-name target treesit-parser-manager-working-directory)))
          (if (file-exists-p target-dir)
              (progn
                (message "Updating %s (%s)" url target)
                (treesit-parser-manager--update target-dir))
            (progn
              (message "Installing %s (%s)" url target)
              (treesit-parser-manager--clone url)))

          (treesitter-grammar-manager--compile-grammar treesit-parser-manager-target-directory target-dir))))))

(defun treesitter-grammar-manager--compile-grammar (destination &optional path)
  "Compile grammar at PATH, and place the resulting shared library in DESTINATION."
  (make-directory destination 'parents)

  (let* ((default-directory
          (expand-file-name "src/" (or path default-directory)))
         (parser-name
          (thread-last (expand-file-name "grammar.json" default-directory)
                       (json-read-file)
                       (alist-get 'name))))
    (message "Compiling grammar at %s" path)

    (with-temp-buffer
      (unless
          (zerop
           (apply #'call-process
                  (if (file-exists-p "scanner.cc")
                      treesit-parser-manager-c++-compiler
                    treesit-parser-manager-c-compiler) nil t nil
                  (append '("parser.c" "-I." "--shared" "-o")
                          (list (expand-file-name
                                 (format "libtree-sitter-%s%s" parser-name module-file-suffix)
                                 destination))
                          treesit-parser-manager-cflags
                          (cond ((file-exists-p "scanner.c") '("scanner.c"))
                                ((file-exists-p "scanner.cc") '("scanner.cc"))))))
        (user-error
         "Unable to compile grammar, please file a bug report\n%s"
         (buffer-string))))
    (message "Completed compilation")))

;;;###autoload
(defun treesit-parser-manager-remove-grammar (&optional repo)
  "Remove REPO and TARGETS."
  (interactive)

  (let* ((repo (or repo (completing-read "Repository: " treesit-parser-manager-grammars)))
         (targets (nth 1 (assoc repo treesit-parser-manager-grammars))))
    (dolist (target targets)
      (let* ((target-dir (expand-file-name target treesit-parser-manager-working-directory))
             (default-directory (expand-file-name "src/" (or target-dir default-directory)))
             (parser-name
              (thread-last (expand-file-name "grammar.json" default-directory)
                           (json-read-file)
                           (alist-get 'name)))
             (lib-file (expand-file-name
                        (format "libtree-sitter-%s%s" parser-name module-file-suffix)
                        treesit-parser-manager-target-directory)))
        (when (file-exists-p target-dir)
          (delete-directory target-dir 'recursive)
          (message "Removed %s" target-dir))

        (when (file-exists-p lib-file)
          (delete-file lib-file)
          (message "Removed %s" lib-file))))

    (setq treesit-parser-manager-grammars
          (delete (cons repo targets) treesit-parser-manager-grammars))))

(provide 'treesit-parser-manager)
;;; treesit-parser-manager.el ends here
